const router = require("express").Router()
const orderModel = require("../Models/orderModels");
const {  UserAuthorization, AdminAuthorization } = require("./verifyToken")

// ============================================CREATE ORDER===================================================//
router.post("/create", UserAuthorization, async (req,res) => {
    
    const newOrder = new orderModel(req.body)
    try{
        const savedOrder = await newOrder.save()
        res.status(200).json(savedOrder);
    }
    catch(err){
        res.status(500).json(err);
    }
    });

// =====================================================UPDATE ORDER==================================//

router.put("/:id", AdminAuthorization, async (req,res) => {
    try{
        const updatedOrder = await orderModel.findByIdAndUpdate(req.params.id, {
            $set: req.body
        }, {new:true});
        res.status(200).json(updatedOrder);
    }
    catch(err){
        res.status(500).json(err);
    }
    });


   // ====================================================DELETE ORDERS==================================//
    router.delete("/:id", AdminAuthorization, async (req,res) => {
        try{
            await orderModel.findByIdAndDelete(req.params.id)
            res.status(200).json("Cart successfully deleted");
        }
        catch(err){
            res.status(500).json(err);
        }
        });



//     // =====================================================GET USER ORDER==================================//
router.get("/find/:userId", UserAuthorization, async (req,res) => {
    try{
        const orders = await orderModel.find({userId: req.params.userId})
        res.status(200).json(orders);
    }
    catch(err){
        res.status(500).json("Youre not allowed to do that");
    }
    });


//     //===============================GET ALL PRODUCT =================================//

    router.get("/", AdminAuthorization, async (req,res) => {
            try{
                const orders = await orderModel.find();
                res.status(200).json(orders);
            }
            catch(err){
                res.status(500).json("Youre not allowed to do that");
            }
            });

// ================================= GET MONTHLY INCOME =============================/

    router.get("/income", AdminAuthorization, async (req,res) => {
    const productId = req.query.pid;
    const date = new Date();
    const lastMonth = new Date(date.setMonth(date.getMonth() - 1));
    const previousMonth = new Date(new Date().setMonth(lastMonth - 1));
    try{
        const income = await orderModel.aggregate([
            {$match: {createdAt: { $gte: previousMonth}, ...(productId && {
                products:{$elemMatch:{productId}},
            }),
        }},
            {
                $project:{
                    month:{$month: "$createdAt"},
                    sales:"$amount",
                },
            },
            {
                $group:{
                    _id:"$month",
                    total:{$sum: "$sales"},
                },
            },
        ]).sort({"_id":1})
        res.status(200).json(income);
    }
    catch(err){
        res.status(500).json(err);
    }
    });


    router.get("/userorders", AdminAuthorization, async (req,res) => {
        try{
            const userOrders = await orderModel.aggregate({
                $lookup:{
                    from:"usermodels",
                    localField:"userId",
                    foreignField:"_id",
                    as:"users_orders"
                }
            })
            res.status(200).json(userOrders);
        }
        catch(err){
            res.status(500).json();
        }
        });


module.exports = router;