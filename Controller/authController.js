const router = require('express').Router();
const userModel = require('../Models/UserModel');
const CryptoJS = require('crypto-js');
const JWT = require('jsonwebtoken');

router.post('/register', async(req,res) => {
    newUser = new userModel({
        username : req.body.username,
        email : req.body.email,
        password : CryptoJS.AES.encrypt(req.body.password, process.env.CRYPTO_Security )
    });
    try
    {   
        savedUser = await newUser.save();
        res.status(200).json(newUser);
    } catch(err){
        res.status(200).json(err);
    }
    
})

router.post('/login', async(req,res) => {
    try
    {   
        currentUser = await userModel.findOne({username: req.body.username});
        !currentUser && res.status(401).json({
            message: "Username doesn't exist"
        });

        const hashedPassword = CryptoJS.AES.decrypt(currentUser.password, process.env.CRYPTO_Security);
        const originalPassword = hashedPassword.toString(CryptoJS.enc.Utf8);
        originalPassword != req.body.password && res.status(401).json({message : "Wrong Password"});
        const accessToken = JWT.sign({
            id:currentUser._id,
            isAdmin:currentUser.isAdmin,
        }, process.env.JWT_Security, {expiresIn:"3d"});

        const { password, ...others} = currentUser._doc;

        return res.status(200).json({...others, accessToken});
    }
    catch(err){
        res.status(500).json(err);
    }
    
})

module.exports = router;