const router = require("express").Router();
const productModel = require("../Models/productModels");
const { UserAuthorization, AdminAuthorization } = require("../Controller/verifyToken")

// ============================================CREATE PRODUCT===================================================//
router.post("/create", AdminAuthorization, async (req,res) => {
    const newProduct = new productModel(req.body)
    try{
        const savedProduct = await newProduct.save()
        res.status(200).json(savedProduct);
    }
    catch(err){
        res.status(500).json(err);
    }
    });

// =====================================================UPDATE PRODUCT==================================//

router.put("/:id", AdminAuthorization, async (req,res) => {
    try{
        const updatedProduct = await productModel.findByIdAndUpdate(req.params.id, {
            $set: req.body
        }, {new:true});
        res.status(200).json(updatedProduct);
    }
    catch(err){
        res.status(500).json(err);
    }
    });


//     // ====================================================DELETE A PRODUCT==================================//
    router.delete("/:id", AdminAuthorization, async (req,res) => {
        try{
            await productModel.findByIdAndDelete(req.params.id)
            res.status(200).json("Product successfully deleted");
        }
        catch(err){
            res.status(500).json(err);
        }
        });



//     // =====================================================GET PRODUCT==================================//
router.get("/:id", async (req,res) => {
    try{
        const product = await productModel.findById(req.params.id)
        res.status(200).json(product);
    }
    catch(err){
        res.status(500).json("Youre not allowed to do that");
    }
    });


//     //===============================GET ALL PRODUCT =================================//

    router.get("/", async (req,res) => {
            const qNew= req.query.new;
            const qCategory = req.query.category;
            try{
                let products;

                if(qNew){
                    products = await productModel.find().sort({createdAt: -1}).limit(5);
                } else if (qCategory){
                    products = await productModel.find({categories:{
                        $in: [qCategory],
                    },});
                } else {
                    products = await productModel.find();
                }
                res.status(200).json(products);
            }
            catch(err){
                res.status(500).json("Youre not allowed to do that");
            }
            });

module.exports = router;