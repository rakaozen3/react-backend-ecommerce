const jwt = require("jsonwebtoken");

const verifyToken = (req,res,next) => {
    const authorization = req.headers.auth;

    if (authorization){
        const token = authorization.split(" ")[1];
        jwt.verify(token, process.env.JWT_Security, (err,user) => {
            if(err){
                res.status(403).json({
                    message: "Token Invalid"
                });
            }else{
                req.user = user;
                next();
                
            }
        })
    }
    else {
        res.status(403).json({
            message :"You're notAuthenticated"
        })
    }
}

const UserAuthorization = (req,res,next) => {
    verifyToken(req,res, () => {
        if (req.user.id === req.params.id || req.user.isAdmin){
            next();
        } else{
            res.status(403).json({
                message:"You're not allowed to do that"
            });
        }
    })
}

const AdminAuthorization = (req,res,next) => {
    verifyToken(req,res, () => {
        if (req.user.isAdmin){
            next()
        } else{
            res.status(403).json({
                message:"You're not allowed to do that"
            });
        }
    })
}

module.exports = {UserAuthorization, AdminAuthorization};