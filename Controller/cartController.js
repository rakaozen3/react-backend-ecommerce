const router = require("express").Router()
const cartModels = require("../Models/cartModels");
const {  UserAuthorization,AdminAuthorization } = require("./verifyToken")

// ============================================CREATE PRODUCT===================================================//
router.post("/create", UserAuthorization, async (req,res) => {
    
    const newCart = new cartModels(req.body)
    try{
        const savedCart = await newCart.save()
        res.status(200).json(savedCart);
    }
    catch(err){
        res.status(500).json(err);
    }
    });

// =====================================================UPDATE PRODUCT==================================//

router.put("/:id", UserAuthorization, async (req,res) => {
    try{
        const updatedCart = await cartModels.findByIdAndUpdate(req.params.id, {
            $set: req.body
        }, {new:true});
        res.status(200).json(updatedCart);
    }
    catch(err){
        res.status(500).json(err);
    }
    });


   // ====================================================DELETE A PRODUCT==================================//
    router.delete("/:id", UserAuthorization, async (req,res) => {
        try{
            await cartModels.findByIdAndDelete(req.params.id)
            res.status(200).json("Cart successfully deleted");
        }
        catch(err){
            res.status(500).json(err);
        }
        });



//     // =====================================================GET PRODUCT==================================//
router.get("/find/:userId", UserAuthorization, async (req,res) => {
    try{
        const cart = await cartModels.findOne({userId: req.params.userId})
        res.status(200).json(cart);
    }
    catch(err){
        res.status(500).json("Youre not allowed to do that");
    }
    });


//     //===============================GET ALL PRODUCT =================================//

    router.get("/", AdminAuthorization, async (req,res) => {
            try{
                const carts = await cartModels.find();
                res.status(200).json(carts);
            }
            catch(err){
                res.status(500).json("Youre not allowed to do that");
            }
            });


module.exports = router;