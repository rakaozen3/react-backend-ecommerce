const router = require('express').Router();
const userModel = require('../Models/UserModel');
const { UserAuthorization, AdminAuthorization } = require('./verifyToken');


router.put('/:id', AdminAuthorization, async(req,res) => {
    if(req.body.password) {
        req.body.password = CryptoJS.AES.encrypt(req.body.password, process.env.SECRET_P).toString();
    }

    try{
        const updatedUser = await userModel.findByIdAndUpdate(req.params.id, 
            {$set: req.body}, {new:true});
        res.status(200).json(updatedUser);
    }catch(err){
        res.status(500).json(err);
    }

})

router.delete("/:id", AdminAuthorization, async (req,res) => {
try{
    await userModel.findByIdAndDelete(req.params.id)
    res.status(200).json("User successfully deleted");
}
catch(err){
    res.status(500).json(err);
}
});


 // =====================================================FIND A USER==================================//
router.get("/find/:id", AdminAuthorization, async (req,res) => {
    try{
        const user = await userModel.findById(req.params.id)
        const { password, ...others } = user._doc;
        res.status(200).json(others);
    }
    catch(err){
        res.status(500).json("Youre not allowed to do that");
    }
    });


    // =====================================================GET ALL USER==================================//

router.get("/stats", AdminAuthorization, async (req,res) => {
    const date = new Date();
    const lastYear = new Date(date.setFullYear(date.getFullYear()-1));
    try{
        const data = await userModel.aggregate([
            {$match: {createdAt:{$gte: lastYear}}},
            {
                $project:{
                    month:{$month:"$createdAt"},
                },
            },
            {
                $group:{
                    _id:"$month",
                    total: {$sum:1},
                },
            },
            {$sort:{_id:1}}
        ])
        res.status(200).json(data);
    }
    catch(err){
        res.status(500).json("Youre not allowed to do that");
    }
    });

    //===============================GET USER STATS =================================//

router.get("/", AdminAuthorization, async (req,res) => {
        const query = req.query.new
        try{
            const users = query ? await userModel.find().sort({_id:-1}).limit(query) : await userModel.find();
            res.status(200).json(users);
        }
        catch(err){
            res.status(500).json("Youre not allowed to do that");
        }
        });


module.exports = router;