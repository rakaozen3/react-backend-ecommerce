const express = require('express');
const dotenv = require('dotenv').config();
const mongoose = require('mongoose')
const port = process.env.PORT_NUMBER;
const cors = require('cors')

const authRoute = require('./Controller/authController');
const userRoute = require('./Controller/userController');
const productRoute = require('./Controller/productController');
const cartRoute = require('./Controller/cartController');
const orderRoute = require('./Controller/orderController');


const app = express()
mongoose
.connect(process.env.MONGODB_URL)
.then(() => {console.log("Database Connected Successfully")})
.catch((err) => {console.log(err)})
// const corsOptions ={
//     origin:'*', 
//     credentials:true,            //access-control-allow-credentials:true
//     optionSuccessStatus:200,
//  }
app.use(cors())
app.use(express.json());       // to support JSON-encoded bodies
app.use(express.urlencoded()); // to support URL-encoded bodies
app.use('/api/users', authRoute);
app.use('/api/users', userRoute);
app.use('/api/products', productRoute);
app.use('/api/cart', cartRoute);
app.use('/api/order', orderRoute);

app.listen(port, () => console.log(`Backend Server is Running on port ${port} `))