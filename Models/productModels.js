const mongoose = require('mongoose');

const productSchema = new mongoose.Schema(
    {
        title:{type: String, required:true, unique:true},
        inStock:{type: String, required:true, unique:true},
        description:{type: String, required:true},
        img:{type: String, required:true},
        categories:{type: Array, required:true},
        merk:{type: String, required:true},
        price:{type: Number, required:true},
        inStock:{type: Boolean, required:true},
    }, {timestamps:true})

module.exports = mongoose.model("productModel", productSchema);